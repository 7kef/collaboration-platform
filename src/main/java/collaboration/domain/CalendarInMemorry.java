package collaboration.domain;

import org.springframework.format.datetime.DateFormatter;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;


public class CalendarInMemorry implements Calendar{
    private List<Event> events = new ArrayList<Event>();
    private DateFormatter df = new DateFormatter("dd.mm.yyyy");


    public CalendarInMemorry() {
        Event event1 = new Event("Event 1 16.06.2018 09:13:30", "Bla bla bla", new Date(Long.parseLong("1529133210000")));
        events.add(event1);
        Event event2 = new Event("Event 2 17.06.2018 13:00:1", "Bla bla bla", new Date(Long.parseLong("1529233210000")));
        events.add(event2);
        Event event3 = new Event("Event 3 20.06.2018 02:56:50", "Bla bla bla", new Date(Long.parseLong("1529456210000")));
        events.add(event3);
        Event event4 = new Event("Event 4 19.06.2018 18:36:50", "Bla bla bla", new Date(Long.parseLong("1529426210000")));
        events.add(event4);
        Event event5 = new Event("Event 5 18.06.2018 13:02:17", "Bla bla bla", new Date(Long.parseLong("1529319737000")));
        events.add(event5);
        Event event6 = new Event("Event 6 20.06.2018 13:02:17", "Bla bla bla", new Date(Long.parseLong("1529467210000")));
        events.add(event6);
        Event event7 = new Event("Event 7 20.06.2018 12:10", "Bla bla bla", new Date(Long.parseLong("1529489421000")));
        events.add(event7);


    }

    public void modifyEvent(Event event){

    }

    public void deleteEvent(Event event){
        events.remove(event);
    }

    public void addEvent(Event event){
        events.add(event);
    }

    public List<Event> getEvents() {
        return events;
    }


    //TODO: Generete constructor, getters & setters, modifyEvent
}
