package collaboration.domain;

public interface Calendar {
    public void modifyEvent(Event event);

    public void deleteEvent(Event event);

    public void addEvent(Event event);
}
