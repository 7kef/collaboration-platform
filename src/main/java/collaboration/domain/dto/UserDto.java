
package collaboration.domain.dto;

import collaboration.domain.CalendarInMemorry;
import collaboration.domain.Task;
import javax.validation.constraints.NotNull;


//import javax.validation.constraints.Email;
import org.hibernate.validator.constraints.Email;
import javax.validation.constraints.Size;


public class UserDto {

    @NotNull
    @Size(min=5, max=16, message="{username.size}")
    private String username;

    @NotNull
    @Size(min=8, max=32, message="{password.size}")
    private String password;
    private String matchingPassword;

    @NotNull
    @Size(min=2, max=30, message="{firstName.size}")
    private String firstName;

    @NotNull
    @Size(min=2, max=30, message="{lastName.size}")
    private String lastName;

    @NotNull
    @Email
    private String email;

    private CalendarInMemorry calendar = new CalendarInMemorry();

    private String profileImage;

    private Task[] tasks;

    public UserDto() {


    }

    public UserDto(@NotNull @Size(min = 5, max = 16, message = "{username.size}") String username,
                   @NotNull @Size(min = 8, max = 32, message = "{password.size}") String password,
                   String matchingPassword,
                   @NotNull @Size(min = 2, max = 30, message = "{firstName.size}") String firstName,
                   @NotNull @Size(min = 2, max = 30, message = "{lastName.size}") String lastName,
                   @NotNull @Email String email, CalendarInMemorry calendar, String profileImage, Task[] tasks) {
        this.username = username;
        this.password = password;
        this.matchingPassword = matchingPassword;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.calendar = calendar;
        this.profileImage = profileImage;
        this.tasks = tasks;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CalendarInMemorry getCalendar() {
        return calendar;
    }

    public void setCalendar(CalendarInMemorry calendar) {
        this.calendar = calendar;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Task[] getTasks() {
        return tasks;
    }

    public void setTasks(Task[] tasks) {
        this.tasks = tasks;
    }
}


