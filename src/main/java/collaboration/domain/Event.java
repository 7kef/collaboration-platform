package collaboration.domain;

import javax.swing.text.DateFormatter;
import java.util.Date;

public class Event {
    private String title;
    private String description;
    private Date dateTime;

    public Event(String title, String description, Date dateTime) {
        this.title = title;
        this.description = description;
        this.dateTime = dateTime;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }


    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    //TODO: Generate constructor, getter & setters
}
