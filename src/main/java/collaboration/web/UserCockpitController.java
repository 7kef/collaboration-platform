package collaboration.web;


import collaboration.data.UserRepository;
import collaboration.domain.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping("/user")
public class UserCockpitController {

    @Autowired
    private UserRepository userRepository;


    @RequestMapping(value="/{username}", method=GET)
    public String showSpitterProfile(@PathVariable String username, Model model) {
        UserDto user = userRepository.findByUsername(username);
        model.addAttribute("user",user);
        return "profile";
    }


}
