package collaboration.data;

import collaboration.domain.CalendarInMemorry;
import collaboration.domain.Task;
import collaboration.domain.dto.UserDto;
import org.springframework.stereotype.Repository;

import java.util.*;
@Repository
public class InMemoryUserRepository implements UserRepository{

    private Set<UserDto> userSet = new HashSet<>();

    public InMemoryUserRepository(){
        UserDto bill = new UserDto("bill","bill1999", "bill1999","Bill", "Gates", "bill@outlook.com", new CalendarInMemorry(),"https://pbs.twimg.com/profile_images/988775660163252226/XpgonN0X_400x400.jpg", new Task[]{new Task("Lorem ipsum")});
        userSet.add(bill);
        UserDto dima = new UserDto("dima","dima1999", "dima1999","Dima", "Nikiforchuk", "dima1999@outlook.com", new CalendarInMemorry(),"https://pbs.twimg.com/profile_images/988775660163252226/XpgonN0X_400x400.jpg", new Task[]{new Task("Lorem ipsum")});
        userSet.add(dima);

    }

    public UserDto save(UserDto user) {
        userSet.add(user);
        return user;
    }

    public UserDto findByUsername(String username) {
        UserDto user=null, temp;
        Iterator<UserDto> userIterator = userSet.iterator();
        while(userIterator.hasNext()){
            temp = userIterator.next();
            if(temp.getUsername().equals(username)) {
                user = temp;
                break;
            }
        }
        if(user==null)
            return null;
        else
            return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InMemoryUserRepository that = (InMemoryUserRepository) o;

        return userSet != null ? userSet.equals(that.userSet) : that.userSet == null;
    }

    @Override
    public int hashCode() {
        return userSet != null ? userSet.hashCode() : 0;
    }
}
