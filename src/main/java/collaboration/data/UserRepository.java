package collaboration.data;

import collaboration.domain.dto.UserDto;

public interface UserRepository {

    UserDto save(UserDto user);

    UserDto findByUsername(String username);
}
