package collaboration.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class WebSecurity {
    public boolean checkUserId(Authentication authentication, String userId) {
				return authentication.getName().equals(userId);
    }
}
