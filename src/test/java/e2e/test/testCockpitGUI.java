package e2e.test;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;


public class testCockpitGUI {
    @Test
    public void cockpitAppearanceCheck() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver/chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebDriverWait wait30 = new WebDriverWait(driver, 30);
        driver.get("http://localhost:8080/");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.get("http://localhost:8080/login");



        By loginForm = By.id("login-form");
        driver.findElement(By.id("userlogin")).sendKeys("bill");
        driver.findElement(By.id("userpassword")).sendKeys("bill1999");


        driver.findElement(By.id("btn-login")).click();
        wait.until(ExpectedConditions.urlMatches(".*/user/bill.*"));

        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //wait.until(ExpectedConditions.urlMatches("/"));

        System.out.println("Quiting application");

        /*By addButton = By.id("add");
        wait.until(ExpectedConditions.presenceOfElementLocated(addButton));
        driver.findElement(addButton).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("back")));
        driver.findElement(By.id("name")).sendKeys("Daenerys Targaryen");
        driver.findElement(By.name("address")).sendKeys("Dragonstone");
        driver.findElement(By.cssSelector("input[ng-model='post.hobbies']")).sendKeys("Break Chains");
        driver.findElement(By.cssSelector(".w3-btn.w3-teal")).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("address")));
        String dataOnPage = driver.getPageSource();
        Assert.assertTrue(dataOnPage.contains("Daenerys Targaryen"));
        Assert.assertTrue(dataOnPage.contains("Dragonstone"));
        Assert.assertTrue(dataOnPage.contains("Break Chains"));*/

        driver.quit();
    }
}
