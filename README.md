# Collaboration platform


## **Collaboration platform** - as the name says - is supposed to be easy-to-use, at the same time it is a flexible and useful tool for managing projects. The flexibility here means the ability to use it for every kind of project.


### The project uses ```Java``` programming language and ```Spring Framework``` for web-development. 
### This project uses MVC architectural pattern. 
### ```Thymleaf``` is used for rendering HTML pages. ```HTML, CSS, Bootstrap``` to create View's base. 

###  I use ```Gradle tool``` for building project.



